# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields, ModelView, Model
from trytond.pool import PoolMeta, Pool
from trytond.pyson import Eval, Bool
from trytond.exceptions import UserError
from trytond.i18n import gettext


class UnitLoad(metaclass=PoolMeta):
    __name__ = 'stock.unit_load'

    load_line = fields.Many2One('carrier.load.order.line', 'Load line',
        select=True, readonly=True,
        ondelete='RESTRICT')
    load_order = fields.Function(
        fields.Many2One('carrier.load.order', 'Load order'),
        'get_load_order', searcher='search_load_order')
    load_order_state = fields.Function(fields.Selection(
        'get_load_order_states', 'Load order State'), 'get_load_order_state')
    load_lines = fields.Many2Many('carrier.load.order.line-stock.unit_load',
        'unit_load', 'load_line', 'Load lines')

    @classmethod
    def __setup__(cls):
        super(UnitLoad, cls).__setup__()
        cls._buttons.update({
            'unload': {
                'invisible': (Eval('load_order_state') == 'done'),
                'readonly': (Eval('load_order_state') == 'done'),
                'depends': ['load_order_state']
            }
        })
        cls._buttons['move_try']['invisible'] |= Bool(Eval('load_order'))
        cls._buttons['move_try'].setdefault('depends', [])
        cls._buttons['move_try']['depends'].append('load_order')

    def get_load_order_state(self, name=None):
        if self.load_order:
            return self.load_order.state

    @classmethod
    def get_load_order_states(cls):
        pool = Pool()
        Load = pool.get('carrier.load.order')
        return Load.state.selection

    @classmethod
    def copy(cls, records, default=None):
        if default is None:
            default = {}
        default = default.copy()
        default['load_line'] = None
        default['load_lines'] = None
        return super(UnitLoad, cls).copy(records, default=default)

    @classmethod
    def delete(cls, records):
        if any(r.load_line for r in records):
            raise UserError(gettext(
                'carrier_load_ul.msg_stock_unit_load_delete_load'))
        super(UnitLoad, cls).delete(records)

    @classmethod
    def get_load_order(cls, records, name=None):
        values = {}
        for record in records:
            load_line = record.load_line or (
                record.load_lines and record.load_lines[-1])
            values[record.id] = load_line.order.id if load_line else None
        return values

    @classmethod
    def search_load_order(cls, name, clause):
        join_operator = 'OR'
        cond1, cond2 = [], []

        def get_field(_field):
            return '%s.%s' % (
                _field,
                clause[0].replace(name, 'order', 1))

        if '.' not in clause[0]:
            if clause[2] is None:
                if clause[1] == '=':
                    join_operator = 'AND'
                cond1.append(('load_line', ) + tuple(clause[1:]))
                cond2.append(('load_lines', ) + tuple(clause[1:]))
            else:
                cond1.append((get_field('load_line'), ) + tuple(clause[1:]))
                cond2.extend([
                    ('load_lines', '!=', None),
                    (get_field('load_lines'), ) + tuple(clause[1:])
                ])
        else:
            cond1.append((get_field('load_line'), ) + tuple(clause[1:]))
            cond2.append((get_field('load_lines'), ) + tuple(clause[1:]))

        return [join_operator,
            cond1,
            cond2
        ]

    @classmethod
    @ModelView.button
    def unload(cls, records, load_order=None):
        if cls.unload_try(records, load_order):
            for record in records:
                if record.load_line:
                    record.load_line = None
                elif record.load_lines:
                    record.load_lines = list(record.load_lines)[:-1]
            cls.save(records)

    @classmethod
    def unload_try(cls, records, load_order=None):
        for record in records:
            load_line = record.load_line
            if load_line and load_order and load_line.order != load_order:
                raise UserError(gettext(
                    'carrier_load_ul.msg_stock_unit_load_wrong_load_order',
                    unit_load=record.rec_name,
                    order=load_order.rec_name))

            if not load_line and record.load_lines:
                load_line = record.load_lines[-1]
                if load_order and load_line.order != load_order:
                    raise UserError(gettext(
                        'carrier_load_ul.msg_stock_unit_load_wrong_load_order',
                        unit_load=record.rec_name,
                        order=load_order.rec_name))

            if load_line and load_line.order.state == 'done' and \
                    load_line.order.sale.state not in ('draft', 'quotation'):
                raise UserError(gettext(
                    'carrier_load_ul.msg_stock_unit_load_unload'))
        return True

    @classmethod
    def get_available(cls, records, name=None):
        res = super(UnitLoad, cls).get_available(records, name)
        sub_records = [r for r in records
            if r.load_line and r.load_line.order.type == 'out']
        res.update({r.id: False for r in sub_records})
        return res

    @classmethod
    def search_available(cls, name, clause):
        res = super(UnitLoad, cls).search_available(name, clause)

        reverse = {
            '=': '!=',
            '!=': '='
        }
        operation = 'AND'
        if (clause[2] and clause[1] == '=') or (
                not clause[2] and clause[1] == '!='):
            operation = 'OR'

        _load_domain = [operation,
            ('load_line', clause[1] if clause[2]
                else reverse[clause[1]], None),
            ('load_line.order.type', clause[1] if not clause[2]
                else reverse[clause[1]], 'out')
        ]
        res.append(_load_domain)
        return res


class UnitLoadLabel(metaclass=PoolMeta):
    __name__ = 'stock.unit_load.label'

    @classmethod
    def counter(cls, unit_load):
        res = super(UnitLoadLabel, cls).counter(unit_load)
        if not res and unit_load.load_line:
            _items = [ul.id for ul in unit_load.load_line.order.unit_loads]
            return '%s' % (_items.index(unit_load.id) + 1)
        return res
