# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import PoolMeta


class LoadOrder(metaclass=PoolMeta):
    __name__ = 'carrier.load.order'

    def _get_inventory_move(self, move):
        move_ = super()._get_inventory_move(move)
        move_.lot = move.lot
        return move_
