# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import Pool
from . import load
from . import unit_load
from . import sale
from . import party
from . import configuration
from . import stock
from . import stock_lot
from . import cmr


def register():
    Pool.register(
        unit_load.UnitLoad,
        load.Configuration,
        load.Load,
        load.LoadOrder,
        load.LoadOrderLine,
        load.LoadUnitLoadOrder,
        load.LoadUnitLoadData,
        load.LoadOrderLineUL,
        sale.Sale,
        sale.SaleLine,
        sale.CreateLoadFromSale,
        sale.CreateLoadLineFromSale,
        load.LoadUnitLoadFailed,
        stock.Move,
        cmr.Template,
        module='carrier_load_ul', type_='model')
    Pool.register(
        load.LoadUnitLoad,
        sale.CreateLoad,
        load.DoLoadOrder,
        module='carrier_load_ul', type_='wizard')
    Pool.register(
        load.LoadSheet,
        load.CMR,
        load.RoadTransportNote,
        load.CarrierLoadPurchase,
        unit_load.UnitLoadLabel,
        module='carrier_load_ul', type_='report')
    Pool.register(
        party.Party,
        load.LoadOrderGrouping,
        configuration.Configuration,
        configuration.ConfigurationLoad,
        module='carrier_load_ul', type_='model',
        depends=['carrier_load_grouping_method'])
    Pool.register(
        stock_lot.LoadOrder,
        module='carrier_load_ul', type_='model',
        depends=['stock_lot'])
