# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.exceptions import UserError, UserWarning


class AddUnitLoadError(UserError):
    pass


class AddUnitLoadWarning(UserWarning):
    pass


class AddUnitLoadOverloadError(AddUnitLoadError):
    pass


class AddUnitLoadOriginError(AddUnitLoadError):
    pass


class AddUnitLoadOriginWarning(AddUnitLoadWarning):
    pass
