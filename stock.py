# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import PoolMeta, Pool
from trytond.transaction import Transaction
from sql import Null
from sql.operators import Concat
from sql.conditionals import Coalesce


class Move(metaclass=PoolMeta):
    __name__ = 'stock.move'

    @classmethod
    def __register__(cls, module_name):
        pool = Pool()
        Location = pool.get('stock.location')
        OrderLine = pool.get('carrier.load.order.line')
        Order = pool.get('carrier.load.order')
        sql_table = cls.__table__()
        location = Location.__table__()
        load_line = OrderLine.__table__()
        load_order = Order.__table__()
        cursor = Transaction().connection.cursor()

        super().__register__(module_name)

        # Migrate from 5.0: link moves to shipment
        cursor.execute(*sql_table.join(
                location, condition=(location.id == sql_table.to_location)
            ).select(sql_table.id,
            where=(
                (sql_table.origin.like('carrier.load.order.line,%')) &
                (Coalesce(sql_table.shipment, '') == '') &
                (location.type == 'customer')
            ), limit=1)
        )
        exists = cursor.fetchone()

        if exists:
            # set shipment to outgoing moves from carrier load order
            cursor.execute(*sql_table.join(
                    location, condition=(location.id == sql_table.to_location)
                ).join(load_line, condition=(
                    Concat('carrier.load.order.line,',
                        load_line.id) == sql_table.origin)
                ).join(load_order, condition=(load_order.id == load_line.order)
                ).select(
                    sql_table.id,
                    load_order.shipment,
                    where=(
                        (sql_table.origin.like('carrier.load.order.line,%')) &
                        (sql_table.shipment == Null) &
                        (location.type == 'customer') &
                        (load_order.shipment != Null) &
                        (load_order.shipment.like('stock.shipment.out,%'))
                ))
            )
            for move_id, shipment in cursor.fetchall():
                cursor.execute(*sql_table.update(
                    columns=[sql_table.shipment],
                    values=[shipment],
                    where=sql_table.id == move_id))

            # set shipment to inventory moves from carrier load order
            # and remove origin
            cursor.execute(*sql_table.join(
                    location, condition=(location.id == sql_table.to_location)
                ).join(load_line, condition=(
                    Concat('carrier.load.order.line,',
                        load_line.id) == sql_table.origin)
                ).join(load_order, condition=(load_order.id == load_line.order)
                ).select(
                    sql_table.id,
                    load_order.shipment,
                    where=(
                        (sql_table.origin.like('carrier.load.order.line,%')) &
                        (sql_table.shipment == Null) &
                        (location.type == 'storage') &
                        (load_order.shipment != Null) &
                        (load_order.shipment.like('stock.shipment.out,%'))
                ))
            )
            for move_id, shipment in cursor.fetchall():
                cursor.execute(*sql_table.update(
                    columns=[sql_table.shipment, sql_table.origin],
                    values=[shipment, Null],
                    where=sql_table.id == move_id))
